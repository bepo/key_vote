﻿#!/usr/bin/python3
#
# key vote common functions
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import colorsys
import sys


keyboards = {
    'azerty mac':       ("""@&é"'(azertqsdfg<wxcvb""", """§è!çà)-yuiop^$hjklmù`n,;:="""),
    'azerty pc':        ("""²&é"'(azertqsdfg<wxcvb""", """-è_çà)=yuiop^$hjklmù*n,;:!"""),
    'bépo 0.6.2.2.4':   ("""@"«»()bépoèauie,êàyh.k""", """_+-/*=%çvdlfzwctsnrm^’qgxj"""),
    'bépo 0.6.2.3':     ("""@"«»()bépoèauie,êàyh.k""", """_+-/*=%çvdlfzwctsnrm^'qgxj"""),
    'bépo 0.6.5.1':     ("""$"«»()bépoèauie,êàyh.k""", """_+-/*=%^vdljzwctsrnmç'qgxf"""),
    'bépo final':       ("""$"«»()bépoèauie,êàyx.k""", """_+-/*=%^vdljzwctsrnmç'qghf""")
}

keyboard_template = """
  ┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────╔════════╗
  │    │    │    │    │    │    │    │    │    │    │    │    │    ║        ║
  │ %(0)s │ %(1)s │ %(2)s │ %(3)s │ %(4)s │ %(5)s │ %(100)s │ %(101)s │ %(102)s │ %(103)s │ %(104)s │ %(105)s │ %(106)s ║ <--    ║
  ╔════╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─╚══╦═════╣
  ║  |<-  ║    │    │    │    │    │    │    │    │    │    │    │    ║   | ║
  ║  ->|  ║ %(6)s │ %(7)s │ %(8)s │ %(9)s │ %(10)s │ %(107)s │ %(108)s │ %(109)s │ %(110)s │ %(111)s │ %(112)s │ %(113)s ║ <-' ║
  ╠═══════╩╗───┴┬───┴┬───┴┬───┴┬───┴┬───┴┬───┴┬───┴┬───┴┬───┴┬───┴┬───╚╗    ║
  ║        ║    │    │    │    │    │    │    │    │    │    │    │    ║    ║
  ║  CAPS  ║ %(11)s │ %(12)s │ %(13)s │ %(14)s │ %(15)s │ %(114)s │ %(115)s │ %(116)s │ %(117)s │ %(118)s │ %(119)s │ %(120)s ║    ║
  ╠══════╦═╝──┬─┴──┬─┴──┬─┴─══─┴──┬─┴──┬─┴─══─┴──┬─┴──┬─┴──┬─┴──╔═╧════╩════╣
  ║   ^  ║    │    │    │    │    │    │    │    │    │    │    ║     ^     ║
  ║   |  ║ %(16)s │ %(17)s │ %(18)s │ %(19)s │ %(20)s │ %(21)s │ %(121)s │ %(122)s │ %(123)s │ %(124)s │ %(125)s ║     |     ║
  ╠══════╩╦═══╧══╦═╧═══╦╧════╧════╧════╧════╧════╧═╦══╧══╦═╧════╬═════╦═════╣
  ║       ║      ║     ║ NBSPACE      FINE-NBSPACE ║     ║      ║     ║     ║
  ║ Ctrl  ║ WinG ║ Alt ║ SPACE                     ║AltGr║ WinD ║WinM ║ Ctrl║
  ╚═══════╩══════╩═════╩═══════════════════════════╩═════╩══════╩═════╩═════╝
"""

if sys.platform == 'win32':  # no (easy) color support
    red_color = ''
    no_color = ''
else:  # terminal color for Linux & MacOSX
    red_color = '\033[31;1m'
    no_color = '\033[0m'


# read the result from the choice
# return the character, retry if needed until a good reply is provided
def read_result(s='', valid_results=None):
    res = input(s)

    if valid_results and res not in valid_results:
        print("Réponse invalide.")
        return read_result(s, valid_results)

    return res


# take a char and return the corresponding integer
# return None if it's not an integer, or if the integer is not 0,1 or 2
def zero_one_two(s):
    if not s.isdigit():
        return None
    i = int(s)
    if 0 <= i <= 2:
        return i
    return None


def print_kbd_layout(keyboard, chars=None):
    d = {}
    for hand_id in (0, 1):
        for (i, c) in enumerate(keyboard[hand_id]):
            if chars is not None and c in chars:  # highlight it
                d[str(i + hand_id * 100)] = red_color + c.upper().rjust(2) + no_color
            else:
                d[str(i + hand_id * 100)] = c.upper().rjust(2)
    print(keyboard_template % d)


def print_kbd_scores(scores, color=False):  # { key1 => score1 ; key2 => score2 ; … }
    d = {}
    for hand_id in (0, 1):
        for (i, c) in enumerate(keyboards['azerty mac'][hand_id]):
            pos = (i + hand_id*100,)
            if pos in list(scores.keys()):
                # define the color
                if color:
                    tmpl = '<span style="color:#%s;">%%s</span>' % "".join(
                        [hex(int(v * 255))[2:].rjust(2, '0')
                            for v in colorsys.hsv_to_rgb(scores[pos] / 3, 1, 0.9)]).upper()
                else:
                    tmpl = "%s"
                if scores[pos] == 1:  # should display '100'
                    d[str(pos[0])] = tmpl % '00'  # we display '00'
                else:
                    d[str(pos[0])] = tmpl % str(int(scores[pos]*100)).rjust(2)
            else:
                d[str(pos[0])] = '  '  # untested keys displayed as spaces
    print(keyboard_template % d)


def print_scores(scores, kbd):  # { key1 => score1 ; key2 => score2 ; … }
    d = {}
    for c, s in scores.items():
        l = d.get(s, [])
        l.append(pos_to_string(c, kbd))
        d[s] = l
    for s in sorted(d.keys()):
        for c in d[s]:
            print("%s: %s" % (c, s))


def choose_kbd():
    print("\n")
    possible_results = list(keyboards.keys())
    for (i, k) in enumerate(keyboards.keys()):
        print(str(i) + ' : ' + k)
        possible_results.append(str(i))
    # print('possible results : ' + str(possible_results))
    kbd_index = read_result("votre clavier: ", possible_results)
    if kbd_index.isdigit():
        return keyboards[list(keyboards.keys())[int(kbd_index)]]
    else:
        return keyboards[kbd_index]


def pos_to_string(pos, kbd):
    ref = kbd[0].ljust(100) + kbd[1]
    s = ''
    for p in pos:
        s += ref[p]
    return s


def string_to_pos(s, kbd):
    ref = kbd[0].ljust(100) + kbd[1]
    pos = []
    for c in s:
        pos.append(ref.find(c))
    return tuple(pos)


def cancel_duel(results, kbd):
    print("Annulation de duel")
    d1 = string_to_pos(read_result("premier duelliste: "), kbd)
    d2 = string_to_pos(read_result("deuxième duelliste: "), kbd)
    ds = tuple(sorted((d1, d2)))
    if ds in results:
        del results[ds]
        print("Le duel est annulé. Vous pourrez revoter pour ce duel plus tard.")
    else:
        print("Le duel n'existe pas.")


def cancel_duels(results, kbd):
    print("Annulation des duels d’un duelliste")
    d1 = string_to_pos(read_result("duelliste: "), kbd)
    for k in list(results.keys()):
        if d1 in k:
            del results[k]


def count_lost(results, scores={}):
    for (pos1, pos2), v in results.items():
        score1 = scores.get(pos1, (0, 0))  # lost, total
        score2 = scores.get(pos2, (0, 0))  # lost, total

        if v != 0:  # not '='
            score1 = (score1[0], score1[1] + 1)  # score1['total']++
            score2 = (score2[0], score2[1] + 1)  # score2['total']++

        if v == 2:  # 1 '<' 2
            score1 = (score1[0] + 1, score1[1])  # score2['lost']++
        elif v == 1:
            score2 = (score2[0] + 1, score2[1])  # score2['lost']++

        scores[pos1] = score1
        scores[pos2] = score2

    return scores


def compute_scores(scores):
    # calcul du score pour chaque touche
    final_scores = {}
    for (pos, (lost, total)) in scores.items():
        if total != 0:
            final_scores[pos] = (total - lost) / total

    return final_scores


def compute_machs(results):
    base_results = {}
    for hand_id in (0, 1):
        less = []
        for (i, result) in enumerate(results[hand_id]):  # pour chaque ensemble de touches
            current_less = []
            for char in result:  # pour chaque touche
                candidate1 = char
                for l in less:
                    candidate2 = l
                    if candidate1 < candidate2:
                        base_results[(candidate1, candidate2)] = 1
                    else:
                        base_results[(candidate2, candidate1)] = 2
                for l in current_less:
                    candidate2 = l
                    if candidate1 < candidate2:
                        base_results[(candidate1, candidate2)] = 0
                    else:
                        base_results[(candidate2, candidate1)] = 0
                current_less.append(char)
            less += current_less
    return base_results
