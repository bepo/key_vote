#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import pickle
import os
import random
import sys

import key_vote_lib


def generate_candidate(candidate_size, hands, wrong=[]):
    candidate = []
    nb_keys = len(hands[0]) + len(hands[1])
    for i in range(candidate_size):
        n = random.randint(0, nb_keys - 1)
        if n >= len(hands[0]):
            n = n - len(hands[0]) + 100
        while n in candidate:
            n = random.randint(0, nb_keys - 1)
            if n >= len(hands[0]):
                n = n - len(hands[0]) + 100
        candidate.append(n)
    if candidate == list(wrong):
        return generate_candidate(candidate_size, hands, wrong)
    return tuple(candidate)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        resultFile = 'result2.duels'
        print("Pas de fichier spécifié, utilisation du fichier {}".format(resultFile))
    else:
        resultFile = sys.argv[1]

    hands = key_vote_lib.chooseKbd()
    left_hand, right_hand = hands

    nb_chars = 2
    nb_votes = 10

    if os.path.exists(resultFile):
        results = pickle.load(open(resultFile, 'rb'))
    else:
        results = {}

    chars = left_hand.ljust(100) + right_hand
    right_home_pos = [(11,), (12,), (13,), (14,)]
    left_home_pos = [(115,), (116,), (117,), (118,)]
    home_pos = right_home_pos + left_home_pos

    run = True
    nb_search = 0

    while run:
        # for min, max in [(0, len(left_hand) - 1)] * nb_votes + [(100, 100 + len(right_hand) - 1)] * nb_votes:
        candidate1 = generate_candidate(nb_chars, hands)
        candidate2 = generate_candidate(nb_chars, hands, candidate1)
        pair = tuple(sorted((candidate1, candidate2)))
        candidate1, candidate2 = pair

        s1 = key_vote_lib.pos_to_string(pair[0], hands)
        s2 = key_vote_lib.pos_to_string(pair[1], hands)

        if pair not in results:
            nb_search = 0
            if candidate1 in home_pos and candidate2 not in home_pos:
                results[pair] = 1
                print("Vote automatique: {} > {}".format(
                    key_vote_lib.pos_to_string(candidate1, hands),
                    key_vote_lib.pos_to_string(candidate2, hands))
                    )
            elif candidate2 in home_pos and candidate1 not in home_pos:
                results[pair] = 2
                print("Vote automatique: {} < {}".format(
                    key_vote_lib.pos_to_string(candidate1, hands),
                    key_vote_lib.pos_to_string(candidate2, hands))
                    )
            elif candidate2 in home_pos and candidate1 in home_pos:
                results[pair] = 0
                print("Vote automatique: {} = {}".format(
                    key_vote_lib.pos_to_string(candidate1, hands),
                    key_vote_lib.pos_to_string(candidate2, hands))
                    )
            else:  # not automatic vote
                # key_vote_lib.printKbdLayout(hands, (s1,s2))

                print()
                print("        Duel:            %(red_color)s%(s1)s%(no_color)s    contre    %(red_color)s%(s2)s%(no_color)s" % {"s1": s1.upper(), "s2": s2.upper(), "red_color": key_vote_lib.red_color, "no_color": key_vote_lib.no_color})
                print()
                print()
                print("%(s1)s ou 1->  %(s1)s     %(s2)s ou 2->  %(s2)s     0->  égalité     Q->  quitter     A->  annuler un duel" % {"s1": s1, "s2": s2})
                print("       %i duels réalisés / 1128 possibles" % len(results))
                res = key_vote_lib.read_result("vote: ", ['Q', '0', '1', '2', s1, s2, 'A'])
                ires = key_vote_lib.zero_one_two(res)

                if res == 'Q':  # Q
                    run = False
                    break
                elif ires is not None:  # 0, 1, 2
                    results[pair] = ires
                elif res == s1:
                    results[pair] = 1
                elif res == s2:
                    results[pair] = 2
                elif res == 'A':
                    key_vote_lib.cancelDuel(results, hands)
                else:
                    print('Erreur dans le programme !')

                pickle.dump(results, open(resultFile, "wb"))
                print()
        else:
            nb_search += 1
            if nb_search > 10000:
                print("Il semble difficile de trouver de nouvelles combinaisons.")
                print("C -> continuer à chercher   Q -> sauver et quitter     A->  annuler un duel")
                print("       %i duels réalisés" % len(results))
                res = key_vote_lib.read_result("Choix: ", ['C', 'Q', 'A'])

                if res == 'Q':
                    run = False
                    break
                elif res == 'C':
                    nb_search = 0
                    print('Continue à chercher')
                elif res == 'A':
                    key_vote_lib.cancelDuel(results, hands)
                else:
                    print('Erreur dans le programme !')

                print()

    pickle.dump(results, open(resultFile, 'wb'))

    # affichage des résultats

    print()
    # print(u'Vos votes:')

    # calcul des matchs perdus/total pour chaque touche
    scores = key_vote_lib.compute_scores(key_vote_lib.count_lost(results))

    # affichage du résultat
    key_vote_lib.print_scores(scores, hands)
    key_vote_lib.print_kbd_scores(scores)
    key_vote_lib.read_result("Appuyez sur Entrée pour quitter…")
