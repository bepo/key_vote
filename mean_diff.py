#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import pickle
import sys

import key_vote_lib


hands = key_vote_lib.choose_kbd()

scores = {}
nb_votes = 0

for filename in sys.argv[2:]:
    f = open(filename, "rb")
    results = pickle.load(f)

    if isinstance(results, tuple):
        results = key_vote_lib.compute_machs(results)

    key_vote_lib.count_lost(results, scores)
    nb_votes += len(results)

    f.close()

ratio = key_vote_lib.compute_scores(scores)

# load the results to compare to the mean
single_results = pickle.load(open(sys.argv[1], "rb"))
if isinstance(single_results, tuple):
    single_results = key_vote_lib.compute_machs(single_results)
single_scores = key_vote_lib.count_lost(single_results)
single_ratio = key_vote_lib.compute_scores(single_scores)

# now, for the all the scores, compute the diff to the mean
single_diff = {}
total = 0
for k, v in single_ratio.items():
    diff = abs(v - ratio[k])
    single_diff[k] = diff
    total += diff

key_vote_lib.print_scores(single_diff, hands)
print()
print("Différence totale: " + str(total))
key_vote_lib.print_kbd_scores(single_diff)
