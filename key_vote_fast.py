#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Damien Thébault <damien.thebault@laposte.net>
#
# Merci à Gaëtan Lehmann pour l'idée originale et pour quelques bouts de code
# Merci à Daniel Delay pour l'idée de la dichotomie
# Merci à Julien Pauty qui a lui aussi travaillé sur le programme
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import pickle
import os
import random
import sys

import key_vote_lib


if __name__ == '__main__':
    results = ([], [])  # tableau contenant les résultats (un pour chaque main)
    matchs = {}  # matchs réellement effectués

    if len(sys.argv) < 2:
        sorted_result_file = 'result-sorted'
        print("Pas de fichier spécifié, utilisation du fichier {}".format(sorted_result_file))
    else:
        sorted_result_file = sys.argv[1]

    if len(sys.argv) < 3:
        result_file = 'result'
        print("Pas de fichier spécifié, utilisation du fichier {}".format(result_file))
    else:
        result_file = sys.argv[2]

    # choix du clavier
    kbd = key_vote_lib.choose_kbd()

    if os.path.exists(sorted_result_file):
        results = pickle.load(open(sorted_result_file, 'rb'))
    else:
        results = ([], [])

    if os.path.exists(result_file):
        matchs = pickle.load(open(result_file, 'rb'))
    else:
        matchs = {}

    # ces touches sont sous les doigts et gagnent toujours
    win = [key_vote_lib.string_to_pos(c, kbd) for c in kbd[0][11:15] + kbd[1][15:19]]
    run = True
    nb_chars = 1

    # tests (pour chaque main)
    for hand_id in (0, 1):
        if not run:
            break
        # création d'une liste contenant tous les caractères mélangés
        untested = []
        for c in kbd[hand_id]:
            # à faire: gérer plusieurs charactères
            untested.append(key_vote_lib.string_to_pos(c, kbd))

    # on enlève ce qu'on a déjà testé (sauvegardé)
    for keys in results[hand_id]:
        for key in keys:
            if key in untested:
                untested.remove(key)
            else:
                print("«{}» n’est pas là!".format(key))
                print(untested)
                print(results[hand_id])

    random.shuffle(untested)

    # tant qu'on a pas testé tous les caractères
    while len(untested) > 0 and run:
        c = untested[0]  # on récupère le premier caractère correspondant
        C = key_vote_lib.pos_to_string(c, kbd)

        if len(results[hand_id]) == 0:  # si le tableau de résultats est vide
            results[hand_id].append((c,))  # on ajoute notre caractère
        else:
            min = 0  # minimum de la dichotomie
            max = len(results[hand_id])-1  # maximum de la dichotomie
            # après le choix, on aura 0 <= c <= len(...)

            while min <= max and run:
                middle = int((max+min)/2)  # on choisit le milieu (dichotomie)

                test = results[hand_id][middle][0]  # on va tester c par rapport à ce caractère
                TEST = key_vote_lib.pos_to_string(test, kbd)

                pair = (c, test)

                if c in win and test in win:  # touches gagnantes au même niveau
                    res = None
                    ires = 0
                elif c in win:  # touches gagnantes gagnent contre toutes les autres touches
                    res = None
                    ires = 1
                elif test in win:  # touches gagnantes gagnent contre les autres touches
                    res = None
                    ires = 2
                else:
                    key_vote_lib.print_kbd_layout(kbd, (C, TEST))
                    print("1: %s  2: %s   0: égalité   Q: quitter  (%i touches restantes sur cette main)" % (C, TEST, len(untested)))
                    res = key_vote_lib.read_result("vote: ", ['0', '1', '2', 'Q', C, TEST])
                    ires = key_vote_lib.zero_one_two(res)

                if res == 'Q':
                    run = False

                if ires == 1 or res == C:  # le caractère est meilleur que le milieu
                    min = middle + 1  # le minimum est maintenant après le milieu
                    if max < min:
                        results[hand_id].insert(middle + 1, (c,))  # insertion avant le milieu
                    matchs[pair] = 1
                elif ires == 2 or res == TEST:  # le caractère est moins bon que le milieu
                    max = middle - 1  # le maximum est avant le milieu
                    if max < min:
                        results[hand_id].insert(middle, (c,))  # insertion après le milieu
                    matchs[pair] = 2
                elif ires == 0:  # les caractères sont aussi bons
                    results[hand_id][middle] += (c,)  # on ajoute au milieu
                    max = min - 1  # fin du test de ce caractère
                    matchs[pair] = 0

            pickle.dump(matchs, open(result_file, 'wb'))
            pickle.dump(results, open(sorted_result_file, 'wb'))

        untested.pop(0)  # on supprime le caractère de la liste

    # génère les duels comme dans la version de base
    base_results = key_vote_lib.compute_machs(results)

    # affichage des résultats
    print()
    # print(u'Vos votes :')

    # calcul des matchs perdus/total pour chaque touche
    scores = key_vote_lib.compute_scores(key_vote_lib.count_lost(base_results))

    # affichage du résultat
    key_vote_lib.print_scores(scores, kbd)
    key_vote_lib.print_kbd_scores(scores)
    key_vote_lib.read_result("Appuyez sur Entrée pour quitter…")
