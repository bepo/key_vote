#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import math
import pickle
import sys

import key_vote_lib


hands = key_vote_lib.choose_kbd()

scores = {}
ratios = []

for filename in sys.argv[1:]:
    f = open(filename, "rb")
    results = pickle.load(f)

    if isinstance(results, tuple):
        results = key_vote_lib.compute_machs(results)

    key_vote_lib.count_lost(results, scores)

    # for the individual ratios
    ratios.append(key_vote_lib.compute_scores(key_vote_lib.count_lost(results)))

    f.close()

ratio = key_vote_lib.compute_scores(scores)

sigma = {}
total = 0
for k in list(ratio.keys()):
    sum2 = 0
    for r in ratios:
        sum2 += math.pow(r[k] - ratio[k], 2)
    s = math.sqrt(sum2 / len(ratios))
    sigma[k] = s
    total += s

key_vote_lib.print_scores(sigma, hands)
print()
print("Différence totale: " + str(total))
key_vote_lib.print_kbd_scores(sigma)
