#!/bin/sh -x
#
# Génère l'archive pour key_vote et la copie sur le site web
#
# Copyright (C) 2008 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#

set -ev

# store the current dir. The image will be stored here.
OUT=$PWD
VERSION=$1

# make a temp dir to create the... temp files
mkdir tmp
pushd tmp

# get the last version
svn export svn://svn.tuxfamily.org/svnroot/dvorak/svn/key_vote/trunk key_vote

# build the archive
zip -r ../key_vote.zip key_vote

popd

scp key_vote.zip ssh.tuxfamily.org:dvorak/dvorak-repository/outils/

# and remove the temp dir
rm -rf tmp
