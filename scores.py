#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import pickle
import sys

import key_vote_lib


hands = key_vote_lib.choose_kbd()
scores = {}
nb_votes = 0

for filename in sys.argv[1:]:
    f = open(filename, "rb")
    results = pickle.load(f)

    if isinstance(results, tuple):
        results = key_vote_lib.compute_machs(results)

    key_vote_lib.count_lost(results, scores)
    nb_votes += len(results)

    f.close()

print(nb_votes, "votes.")
ratio = key_vote_lib.compute_scores(scores)
key_vote_lib.print_scores(ratio, hands)
