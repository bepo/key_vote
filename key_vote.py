#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


import pickle
import os
import random
import sys

import key_vote_lib


def generate_candidate(candidate_size, min, max, wrong=[]):
    candidate = []
    for i in range(candidate_size):
        n = random.randint(min, max)
        while n in candidate:
            n = random.randint(min, max)
        candidate.append(n)
    if candidate == list(wrong):
        return generate_candidate(candidate_size, min, max, wrong)
    return tuple(candidate)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        result_file = 'result'
        print("Pas de fichier spécifié, utilisation du fichier " + result_file)
    else:
        result_file = sys.argv[1]

    hands = key_vote_lib.choose_kbd()
    left_hand, right_hand = hands

    nb_chars = 1

    nb_votes = 10

    if os.path.exists(result_file):
        results = pickle.load(open(result_file, "rb"))
    else:
        results = {}

    chars = left_hand.ljust(100) + right_hand
    rightHomePos = [(11,), (12,), (13,), (14,)]
    leftHomePos = [(115,), (116,), (117,), (118,)]
    homePos = rightHomePos + leftHomePos

    run = True
    nb_search = 0

    while run:
        for min, max in [(0, len(left_hand) - 1)] * nb_votes + [(100, 100 + len(right_hand) - 1)] * nb_votes:
            candidate1 = generate_candidate(nb_chars, min, max)
            candidate2 = generate_candidate(nb_chars, min, max, candidate1)
            pair = tuple(sorted((candidate1, candidate2)))
            candidate1, candidate2 = pair

            s1 = key_vote_lib.pos_to_string(pair[0], hands)
            s2 = key_vote_lib.pos_to_string(pair[1], hands)

            if pair not in results:
                nb_search = 0
                if candidate1 in homePos and candidate2 not in homePos:
                    results[pair] = 1
                    print("Vote automatique: {} > {}".format(
                        key_vote_lib.pos_to_string(candidate1, hands),
                        key_vote_lib.pos_to_string(candidate2, hands))
                        )
                elif candidate2 in homePos and candidate1 not in homePos:
                    results[pair] = 2
                    print("Vote automatique: {} < {}".format(
                        key_vote_lib.pos_to_string(candidate1, hands),
                        key_vote_lib.pos_to_string(candidate2, hands))
                        )
                elif candidate2 in homePos and candidate1 in homePos:
                    results[pair] = 0
                    print("Vote automatique: {} = {}".format(
                        key_vote_lib.pos_to_string(candidate1, hands),
                        key_vote_lib.pos_to_string(candidate2, hands))
                        )
                else:  # not automatic vote
                    key_vote_lib.printKbdLayout(hands, (s1, s2))

                    print()
                    print("        Duel :            %(redColor)s%(s1)s%(noColor)s    contre    %(redColor)s%(s2)s%(noColor)s" % {"s1": s1.upper(), "s2": s2.upper(), "redColor": key_vote_lib.redColor, "noColor": key_vote_lib.noColor})
                    print()
                    print()
                    print("%(s1)s ou 1->  %(s1)s     %(s2)s ou 2->  %(s2)s     0->  égalité     Q->  quitter     A->  annuler un duel     T->  annuler des duels d'une touche" % {"s1": s1, "s2": s2})
                    print("       %i duels réalisés / 556 possibles" % len(results))
                    res = key_vote_lib.readResult('vote: ', ['Q', '0', '1', '2', s1, s2, 'A', 'T'])
                    ires = key_vote_lib.zeroOneTwo(res)

                    if res == 'Q':  # Q
                        run = False
                        break
                    elif ires is not None:  # 0, 1, 2
                        results[pair] = ires
                    elif res == s1:
                        results[pair] = 1
                    elif res == s2:
                        results[pair] = 2
                    elif res == 'A':
                        key_vote_lib.cancelDuel(results, hands)
                    elif res == 'T':
                        key_vote_lib.cancelDuels(results, hands)
                    else:
                        print("Erreur dans le programme!")

                    pickle.dump(results, open(result_file, "wb"))
                    print()
            else:
                nb_search += 1
                if nb_search > 10000:

                    print("Il semble difficile de trouver de nouvelles combinaisons.")
                    print("C -> continuer à chercher   Q -> sauver et quitter     A->  annuler un duel     T->  annuler des duels d'une touche")
                    print("       %i duels réalisés" % len(results))
                    res = key_vote_lib.readResult("Choix: ", ['C', 'Q', 'A', 'T'])

                    if res == 'Q':
                        run = False
                        break
                    elif res == 'C':
                        nb_search = 0
                        print('Continue à chercher')
                    elif res == 'A':
                        key_vote_lib.cancelDuel(results, hands)
                    elif res == 'T':
                        key_vote_lib.cancelDuels(results, hands)
                    else:
                        print("Erreur dans le programme !")

                    print()

    pickle.dump(results, open(result_file, "wb"))

    # affichage des résultats

    print()
    # print(u'Vos votes :')

    # calcul des matchs perdus/total pour chaque touche
    scores = key_vote_lib.computeScores(key_vote_lib.countLost(results))

    # affichage du résultat
    key_vote_lib.printScores(scores, hands)
    key_vote_lib.printKbdScores(scores)
    key_vote_lib.readResult("Appuyez sur Entrée pour quitter")
