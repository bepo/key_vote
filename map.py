#!/usr/bin/python3
#
# Définition d'une carte d'accessibilité par duel de touches
# Copyright (C) 2007 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.


from optparse import OptionParser
import pickle

import key_vote_lib


parser = OptionParser(usage='"Usage: map.py ..."')
parser.add_option("-c", "--color", action="store_true", dest="color",
                  help="Colored output for mediawiki (not implemented).")
(opts, args) = parser.parse_args()

# check the arguments
if len(args) < 1:
    parser.error("incorrect number of arguments")

scores = {}
nb_votes = 0

for filename in args:
    f = open(filename, "rb")
    results = pickle.load(f)

    if isinstance(results, tuple):
        results = key_vote_lib.compute_machs(results)

    key_vote_lib.count_lost(results, scores)
    nb_votes += len(results)

    f.close()

print(len(args), "participants,", nb_votes, "votes.")
ratio = key_vote_lib.compute_scores(scores)
key_vote_lib.print_kbd_scores(ratio, opts.color)
